{
  "entities": [
    {
      "data": [
        {
    	  "NK_Message" : "-- BLANK -- NUM_MONTH EMP_128--",
		  "EXP_COMP2": "1000",
          "EMP_125": "10",
          "YOUR_EEID": "101",
          "EMP_129": "",
          "EMP_128": ""
        },
		{
    	  "NK_Message" : "-- Empty -- NUM_MONTH EMP_128 ",
		  "EXP_COMP2": "1000",
          "EMP_125": "10",
          "YOUR_EEID": "101",
          "EMP_129": ""
        },
        {
    	  "NK_Message" : "-- INVALID -- NUM_MONTH EMP_128 ",
		  "EXP_COMP2": "1000",
          "EMP_125": "10",
          "YOUR_EEID": "101",
          "EMP_129": "",
          "EMP_128": "AA10"
        },
		{
    	  "NK_Message" : "-- NULL -- NUM_MONTH_128   ",
		  "EXP_COMP2": "1000",
          "EMP_125": "10",
          "YOUR_EEID": "101",
          "EMP_129": "",
          "EMP_128": null
        },
        {
    	  "NK_Message" : "-- INVALID -- MONTH_BASE_EMP_125  ",
		  "EXP_COMP2": "1000",
          "EMP_125": "A10",
          "YOUR_EEID": "101",
          "EMP_129": "",
          "EMP_128": "10"
        },
		 {
    	  "NK_Message" : "-- NULL -- MONTH_BASE_EMP_125  ",
		  "EXP_COMP2": "1000",
          "EMP_125": null,
          "YOUR_EEID": "101",
          "EMP_129": "",
          "EMP_128": "10"
        },
		{
    	  "NK_Message" : "-- EMPTY -- MONTH_BASE_EMP_125  ",
		  "EXP_COMP2": "1000",
          "YOUR_EEID": "101",
          "EMP_129": "",
          "EMP_128": "10"
        },
		{
    	  "NK_Message" : "-- BLANK-- MONTH_BASE_EMP_125  ",
		  "EXP_COMP2": "1000",
		  "EMP_125": "",
          "YOUR_EEID": "101",
          "EMP_129": "",
          "EMP_128": "10"
        },
        {
    	  "NK_Message" : "-- INVALID -- ANNUAL_BASE_EMP_129   ",
		  "EXP_COMP2": "1000",
          "EMP_125": "10",
          "YOUR_EEID": "101",
          "EMP_129": "A78",
          "EMP_128": "10"
        },
		{
    	  "NK_Message" : "-- BLANK -- ANNUAL_BASE_EMP_129   ",
		  "EXP_COMP2": "1000",
          "EMP_125": "10",
          "YOUR_EEID": "101",
          "EMP_129": "",
          "EMP_128": "10"
        },
		{
    	  "NK_Message" : "--EMPTY -- ANNUAL_BASE_EMP_129   ",
		  "EXP_COMP2": "1000",
          "EMP_125": "10",
          "YOUR_EEID": "101",
          "EMP_128": "10"
        },
        {
    	  "NK_Message" : "-- INVALID -- POSCODE --",
		  "EXP_COMP2": "1000",
          "EMP_125": "10",
          "YOUR_EEID": "101",
          "EMP_129": "",
          "EMP_128": "10"
        },
        {
    	  "NK_Message" : "-- Blank -- POSCODE --",
		  "EXP_COMP2": "1000",
          "EMP_125": "10",
          "YOUR_EEID": "101",
          "EMP_129": "",
          "EMP_128": "10"
        },
		{
    	  "NK_Message" : "-- Empty -- POSCODE --",
		  "EXP_COMP2": "1000",
          "EMP_125": "10",
          "YOUR_EEID": "101",
          "EMP_129": "",
          "EMP_128": "10"
        },
        {
		  "EXP_COMP2": "1000",
          "EMP_125": "10",
          "YOUR_EEID": "101",
          "EMP_129": "",
          "EMP_128": "10"
        },
        {
		  "EXP_COMP2": "1000",
          "EMP_125": "32000",
          "YOUR_EEID": "102",
          "EMP_129": "101"
        },
        {
		  "EXP_COMP2": "1000",
          "EMP_125": "32000",
          "YOUR_EEID": "103",
          "EMP_129": "99"
        },
        {
		  "EXP_COMP2": "1000",
          "EMP_125": "32000",
          "YOUR_EEID": "104",
          "EMP_129": "98"
        },
        {
		  "EXP_COMP2": "1000",
          "EMP_125": "32000",
          "YOUR_EEID": "105",
          "EMP_129": "199"
        },
        {
		  "EXP_COMP2": "1000",
          "EMP_125": "32000",
          "YOUR_EEID": "106",
          "EMP_129": "201"
        },
        {
		  "EXP_COMP2": "1000",
          "EMP_125": "32000",
          "YOUR_EEID": "107",
          "EMP_129": "52"
        },
        {
		  "EXP_COMP2": "1000",
          "EMP_125": "32000",
          "YOUR_EEID": "108",
          "EMP_129": "48"
        },
        {
		  "EXP_COMP2": "1000",
          "EMP_125": "32000",
          "YOUR_EEID": "109",
          "EMP_129": "40"
        }
      ],
      "sectionStructure": {
        "columns": [
          {
            "code": "EMP_125",
            "displayLabel": "Monthly Base Salary (Equivalent to 100% work time)",
            "dataType": "int",
            "questionType": "integer",
            "validations": [
              {
                "errorType": "ERROR",
                "validationType": "expression",
                "expression": "INVERSIONCHECK(XAVG(\"EMP_125\", \"EMP_125 >= 10\" , \"POS_CLASS\"));",
                "errorGroup": "Personal",
                "message": "Length of Department"
              }
            ]
          }
        ]
      },
      "otherSectionsData": {
        
      },
      "contextData": {
        "campaignId": "599820d4747cf240d90c0b8c",
        "companyId": "599820d4747cf240d90c0b8d",
        "sectionId": "incumbent_data",
        "grpCode": "",
        "cpyCode": "",
        "ctryCode": "US",
        "orgSize": 20,
        "uniqueIdColumnCode": "YOUR_EEID",
        "industry": {
          "superSector": "HT",
          "sector": "196",
          "subSector": "2070"
        }
      }
    }
  ]
}







{
  "entities": [
    {
      "data": [
        {
    	  "NK_Message" : "-- BLANK -- NUM_MONTH EMP_128--",
		  "EXP_COMP2": "1000",
          "EMP_125": "10",
          "YOUR_EEID": "101",
          "EMP_129": "",
          "EMP_128": ""
        },
		{
    	  "NK_Message" : "-- Empty -- NUM_MONTH EMP_128 ",
		  "EXP_COMP2": "1000",
          "EMP_125": "10",
          "YOUR_EEID": "101",
          "EMP_129": ""
         
        },
        {
    	  "NK_Message" : "-- INVALID -- NUM_MONTH EMP_128 ",
		  "EXP_COMP2": "1000",
          "EMP_125": "10",
          "YOUR_EEID": "101",
          "EMP_129": "",
          "EMP_128": "AA10"
        },
		{
    	  "NK_Message" : "-- NULL -- NUM_MONTH_128   ",
		  "EXP_COMP2": "1000",
          "EMP_125": "10",
          "YOUR_EEID": "101",
          "EMP_129": "",
          "EMP_128": null
        },
        {
    	  "NK_Message" : "-- INVALID -- MONTH_BASE_EMP_125  ",
		  "EXP_COMP2": "1000",
          "EMP_125": "A10",
          "YOUR_EEID": "101",
          "EMP_129": "",
          "EMP_128": "10"
        },
		 {
    	  "NK_Message" : "-- NULL -- MONTH_BASE_EMP_125  ",
		  "EXP_COMP2": "1000",
          "EMP_125": null,
          "YOUR_EEID": "101",
          "EMP_129": "",
          "EMP_128": "10"
        },
		{
    	  "NK_Message" : "-- EMPTY -- MONTH_BASE_EMP_125  ",
		  "EXP_COMP2": "1000",
          "YOUR_EEID": "101",
          "EMP_129": "",
          "EMP_128": "10"
        },
		{
    	  "NK_Message" : "-- BLANK-- MONTH_BASE_EMP_125  ",
		  "EXP_COMP2": "1000",
		  "EMP_125": "",
          "YOUR_EEID": "101",
          "EMP_129": "",
          "EMP_128": "10"
        },
        {
    	  "NK_Message" : "-- INVALID -- ANNUAL_BASE_EMP_129   ",
		  "EXP_COMP2": "1000",
          "EMP_125": "10",
          "YOUR_EEID": "101",
          "EMP_129": "A78",
          "EMP_128": "10"
        },
		{
    	  "NK_Message" : "-- BLANK -- ANNUAL_BASE_EMP_129   ",
		  "EXP_COMP2": "1000",
          "EMP_125": "10",
          "YOUR_EEID": "101",
          "EMP_129": "",
          "EMP_128": "10"
        },
		{
    	  "NK_Message" : "--EMPTY -- ANNUAL_BASE_EMP_129   ",
		  "EXP_COMP2": "1000",
          "EMP_125": "10",
          "YOUR_EEID": "101",
          "EMP_128": "10"
        },
        {
    	  "NK_Message" : "-- INVALID -- POSCODE --",
		  "EXP_COMP2": "1000",
          "EMP_125": "10",
          "YOUR_EEID": "101",
          "EMP_129": "",
          "EMP_128": "10"
        },
        {
    	  "NK_Message" : "-- Blank -- POSCODE --",
		  "EXP_COMP2": "1000",
          "EMP_125": "10",
          "YOUR_EEID": "101",
          "EMP_129": "",
          "EMP_128": "10"
        },
		{
    	  "NK_Message" : "-- Empty -- POSCODE --",
		  "EXP_COMP2": "1000",
          "EMP_125": "10",
          "YOUR_EEID": "101",
          "EMP_129": "",
          "EMP_128": "10"
        },
        {
		  "EXP_COMP2": "1000",
          "EMP_125": "10",
          "YOUR_EEID": "101",
          "EMP_129": "",
          "EMP_128": "10"
        },
        {
		  "EXP_COMP2": "1000",
          "EMP_125": "32000",
          "YOUR_EEID": "102",
          "EMP_129": "101"
        },
        {
		  "EXP_COMP2": "1000",
          "EMP_125": "32000",
          "YOUR_EEID": "103",
          "EMP_129": "99"
        },
        {
		  "EXP_COMP2": "1000",
          "EMP_125": "32000",
          "YOUR_EEID": "104",
          "EMP_129": "98"
        },
        {
		  "EXP_COMP2": "1000",
          "EMP_125": "32000",
          "YOUR_EEID": "105",
          "EMP_129": "199"
        },
        {
		  "EXP_COMP2": "1000",
          "EMP_125": "32000",
          "YOUR_EEID": "106",
          "EMP_129": "201"
        },
        {
		  "EXP_COMP2": "1000",
          "EMP_125": "32000",
          "YOUR_EEID": "107",
          "EMP_129": "52"
        },
        {
		  "EXP_COMP2": "1000",
          "EMP_125": "32000",
          "YOUR_EEID": "108",
          "EMP_129": "48"
        },
        {
		  "EXP_COMP2": "1000",
          "EMP_125": "32000",
          "YOUR_EEID": "109",
          "EMP_129": "40"
        }
      ],
      "sectionStructure": {
        "columns": [
          {
            "code": "EMP_125",
            "displayLabel": "Monthly Base Salary (Equivalent to 100% work time)",
            "dataType": "int",
            "questionType": "integer",
            "validations": [
              {
                "errorType": "ERROR",
                "validationType": "expression",
                "expression": "INVERSIONCHECK(XAVG(\"EMP_125\", \"EMP_125 >= 10\" , \"POS_CLASS\"));",
                "errorGroup": "Personal",
                "message": "Length of Department"
              }
            ]
          }
        ]
      },
      "otherSectionsData": {
        
      },
      "contextData": {
        "campaignId": "599820d4747cf240d90c0b8c",
        "companyId": "599820d4747cf240d90c0b8d",
        "sectionId": "incumbent_data",
        "grpCode": "",
        "cpyCode": "",
        "ctryCode": "US",
        "orgSize": 20,
        "uniqueIdColumnCode": "YOUR_EEID",
        "industry": {
          "superSector": "HT",
          "sector": "196",
          "subSector": "2070"
        }
      }
    }
  ]
}