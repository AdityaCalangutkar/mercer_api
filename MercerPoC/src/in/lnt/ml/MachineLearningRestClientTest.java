package in.lnt.ml;

import org.json.JSONObject;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

public class MachineLearningRestClientTest {
	private static final Logger logger = LoggerFactory.getLogger(MachineLearningRestClientTest.class);
	
	@Test
	public void test_successful_response() {
		//JSON OBject to pass.
		JSONObject json = new JSONObject();
		//Creation of rest template.
	    RestTemplate restTemplate = new RestTemplate();
	    //JSON input String as java string.
	    String requestJson =  "{\"entities\":[{\"data\":[{\"YOUR_OTHER\":\"\",\"YOUR_TITLE\":\"C7\",\"CLIENT_EXCLUDE_EMPLOYEE\":\"C14\",\"ECONOMIC_VALUE\":null,\"$$id\":\"5a5498630192b164d95c9309\",\"YOUR_LEVEL\":\"C9\",\"POS_CLASS_INGESTED\":null,\"POS_TITLE\":\"Legal Counsel: General Business (Shared Services & Outsourcing) - Executive 1\",\"OLD_POSCODE\":\"C3\",\"YOUR_CODE\":\"C8\",\"EMP_001\":\"C1\",\"TEST_TEST\":\"C12\",\"SYSTEM_EXCLUDE_EMPLOYEE\":\"C13\",\"POS_CODE\":\"LCQ.03.010.E10\",\"YOUR_GRADE\":\"C10\",\"TEST_5678\":null,\"validationErrors\":[{\"field\":\"TEST_TEST\",\"validationType\":\"expression\",\"errorType\":\"ERROR\",\"message\":\"(2018- nvl(POSDATA.BIRTH_YEAR,0) < POSDATA.COUNTRY.AGE_HIRE_MIN) .or. POSDATA.BIRTH_YEAR <1941null\",\"category\":\"input_data\"}]},{\"YOUR_OTHER\":\"\",\"YOUR_TITLE\":\"D7\",\"CLIENT_EXCLUDE_EMPLOYEE\":\"D14\",\"ECONOMIC_VALUE\":null,\"POS_CLASS_ADJUST\":null,\"$$id\":\"5a5498630192b164d95c930a\",\"YOUR_LEVEL\":\"D9\",\"POS_CLASS_INGESTED\":null,\"POS_TITLE\":null,\"OLD_POSCODE\":\"D3\",\"YOUR_CODE\":\"D8\",\"POS_CLASS\":null,\"EMP_001\":\"D1\",\"TEST_TEST\":\"D12\",\"SYSTEM_EXCLUDE_EMPLOYEE\":\"D13\",\"POS_CODE\":null,\"YOUR_GRADE\":\"D10\",\"TEST_5678\":null,\"validationErrors\":[{\"field\":\"TEST_TEST\",\"validationType\":\"expression\",\"errorType\":\"ERROR\",\"message\":\"(2018- nvl(POSDATA.BIRTH_YEAR,0) < POSDATA.COUNTRY.AGE_HIRE_MIN) .or. POSDATA.BIRTH_YEAR <1941null\",\"category\":\"input_data\"}]}],\"contextData\":{\"uniqueIdColumnCode\":\"YOUR_EEID\",\"companyId\":\"5a53bb7dc37a61145f75d6be\",\"campaignId\":\"5a53320f388a27001570b45a\",\"grpCode\":\"\",\"companyName\":\"Super Awesome company\",\"cpyCode\":\"\",\"industry\":{\"superSector\":\"LS\",\"subSector\":\"1050\",\"sector\":\"106\"},\"sectionId\":\"EMP_DATA\",\"orgSize\":null,\"ctryCode\":\"AF\"}}]}";
	    //put all data into json map
	    json.put("MetaData", requestJson);
	    json.put("Data", requestJson);
	    //Create http headers
	    HttpHeaders headers = new HttpHeaders();
	    //Set content type as JSON
	    headers.setContentType(MediaType.APPLICATION_JSON);
	    HttpEntity<String> entity = new HttpEntity<String>(json.toString() ,headers);
	    try {
	    	//Fire the API call with URL and json object
	    	
	    	 restTemplate.postForObject("http://13.85.25.149:5000/upload", entity, JSONObject.class);
	    } catch (Exception ex) {
	    	// Log the error.
	    	logger.info(ex.getMessage());
	    	
	    }
	    
	}
}
