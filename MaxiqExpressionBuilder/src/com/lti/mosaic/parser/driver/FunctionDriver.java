package com.lti.mosaic.parser.driver;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lti.mosaic.antlr4.parser.ExpressionParser;
import com.lti.mosaic.function.def.AggregateFunctions;
import com.lti.mosaic.function.def.ArithmeticFunctions;
import com.lti.mosaic.function.def.CustomFunctions;
import com.lti.mosaic.function.def.DateFunctions;
import com.lti.mosaic.function.def.InversionAndRangeCheckFunctions;
import com.lti.mosaic.function.def.StringFunctions;
import com.lti.mosaic.function.def.Value;
import com.lti.mosaic.parser.EvalVisitorHelper;
import com.lti.mosaic.parser.enums.FunctionEnum;

public class FunctionDriver {
	
	private static final Logger logger = LoggerFactory.getLogger(FunctionDriver.class);

	private AggregateFunctions aggregateFunctions = null;
	private Map<String, Value> aggregateFunctionCache = null;
	private InversionAndRangeCheckFunctions inversionAndRangeCheckFunctions = null;
	private Map<String, ArrayList<String[]>> inputDataMaster = new HashMap<>();
	private List<String[]> data = new ArrayList<>();
	private Map<String, Object> otherData = new HashMap<>();
	private String expression;
	
	public AggregateFunctions getAggregateFunctions() {
		return aggregateFunctions;
	}

	public void setAggregateFunctions(AggregateFunctions aggregateFunctions) {
		this.aggregateFunctions = aggregateFunctions;
	}

	public Map<String, Value> getAggregateFunctionCache() {
		return aggregateFunctionCache;
	}

	public void setAggregateFunctionCache(Map<String, Value> aggregateFunctionCache) {
		this.aggregateFunctionCache = aggregateFunctionCache;
	}

	public InversionAndRangeCheckFunctions getInversionAndRangeCheckFunctions() {
		return inversionAndRangeCheckFunctions;
	}

	public void setInversionAndRangeCheckFunctions(InversionAndRangeCheckFunctions inversionAndRangeCheckFunctions) {
		this.inversionAndRangeCheckFunctions = inversionAndRangeCheckFunctions;
	}

	public Map<String, ArrayList<String[]>> getInputDataMaster() {
		return inputDataMaster;
	}

	public void setInputDataMaster(Map<String, ArrayList<String[]>> inputDataMaster) {
		this.inputDataMaster = inputDataMaster;
	}

	public List<String[]> getData() {
		return data;
	}

	public void setData(List<String[]> data) {
		this.data = data;
	}

	public Map<String, Object> getOtherData() {
		return otherData;
	}

	public void setOtherData(Map<String, Object> otherData) {
		this.otherData = otherData;
	}

	public String getExpression() {
		return expression;
	}

	public void setExpression(String expression) {
		this.expression = expression;
	}

	public FunctionDriver(Map<String, ArrayList<String[]>> inputDataMaster, List<String[]> data,
			Map<String, Object> otherData, Map<String, Value> aggregateFunctionCache, String expression) {
		this.data = data;
		this.otherData = otherData;
		this.inputDataMaster = inputDataMaster;
		this.aggregateFunctionCache = aggregateFunctionCache;
		this.expression = expression;
	}

	public Value executeFunction(String functionName, List<Value> args) {

		printArgs(functionName, args);
		
 		try{
		switch (FunctionEnum.valueOf(functionName)) {
		case FIND_IN_SET:
				return StringFunctions.findInSet(args.get(0), args);
		case CONCAT:
				return StringFunctions.concat((args.get(0).asString()), (args.get(1).asString()));
		case LENGTH:
				return StringFunctions.length(args.get(0));
		case REPEAT:
				return StringFunctions.repeat(args.get(0).asString(), args.get(1).asInteger());
		case INSTR:
				return StringFunctions.instr(args.get(0).asString(), args.get(1).asString());
		case LOCATE:
				return StringFunctions.locate(args.get(0).asString(), args.get(1).asString(), args.get(2).asInteger());
		case LOWER:
				return StringFunctions.lower((args.get(0).asString()));
		case UPPER:
				return StringFunctions.upper(args.get(0).asString());
		case REGEXP_EXTRACT:
				return StringFunctions.regexpExtract(args.get(0).asString(), args.get(1).asString(),
					args.get(2).asInteger());
		case REGEXP_REPLACE:
				return StringFunctions.regexpReplace(args.get(0).asString(), args.get(1).asString(),
					args.get(2).asString());
		case REVERSE:
				return StringFunctions.reverse(args.get(0).asString());
		case RPAD:
				return StringFunctions.rpad(args.get(0).asString(), args.get(1).asInteger(), args.get(2).asString());
		case LPAD:
				return StringFunctions.lpad(args.get(0).asString(), args.get(1).asInteger(), args.get(2).asString());
		case LTRIM:
				return StringFunctions.ltrim(args.get(0).asString());
		case RTRIM:
				return StringFunctions.rtrim(args.get(0).asString());
		case SPACE:
				return StringFunctions.space(args.get(0).asInteger());
		case INITCAP:
				return StringFunctions.initCap(args.get(0).asString());
		case SOUNDEX:
				return StringFunctions.soundex(args.get(0).asString());
		case LEVENSHTEIN:
				return StringFunctions.levenshtein(args.get(0).asString(), args.get(1).asString());
		case SUBSTR:
				return StringFunctions.substr(args.get(0), args.get(1), args.get(2));
		case TRANSLATE:
				return StringFunctions.translate(args.get(0).asString(), args.get(1).asString(), args.get(2).asString());
		case TRIM:
				return StringFunctions.trim((args.get(0).asString()));
		case REGEX_MATCH:
				return StringFunctions.regexMatch(args.get(0).asString(), args.get(1).asString());
		case ISBLANK:
				if (null == args.get(0)) {
					return new Value(true);
				} else {
					return StringFunctions.isBlank(args.get(0));
				}
		case ISNOTBLANK:
				if (null == args.get(0)) {
					return new Value(false);
				} else {
					return StringFunctions.isNotBlank(args.get(0));
				}
		case DECODE:
				return CustomFunctions.decode(args);
		case SUBSTRP:
				return StringFunctions.substrp(args.get(0).asString(), args.get(1).asInteger(), args.get(2).asInteger());
		case MID:
				return StringFunctions.mid(args.get(0), args.get(1), args.get(2));
		case SPLIT:
				return StringFunctions.split(args.get(0).asString(), args.get(1).asString());

		// Date Functions
		case TO_DATE: 
				return DateFunctions.toDate(args.get(0).asString(), args.get(1).asString());
		case YEAR:
				return DateFunctions.year(args.get(0).asDate());
		case NOW:
				return DateFunctions.now();
		case ADD_MONTHS:
				return DateFunctions.addMonths(args.get(0).asDate(), args.get(1).asInteger());
		case DATEDIFF:
				return DateFunctions.dateDiff(args.get(0).asDate(), args.get(1).asDate());

		// F(x)
		case BOOL_IN_LOOKUP:
				return CustomFunctions.boolInLookup(args.get(0).asString(), args.get(1).asString());
		case LOOK_UP_CHECK:
				return CustomFunctions.lookUpCheck(args.get(0).asString(), args.get(1).asString(), args.get(2).asString());
		case LOOKUP:
				return CustomFunctions.lookup(args.get(0).asString(), args.get(1).asString(), args.get(2).asString(),args.get(3).asString());
		case LOOKUPMAP:
				return CustomFunctions.lookUpMap(args.get(0).asString(), args.get(1).asString(), args.get(2).asString());
			
		case NVL:
				return CustomFunctions.nvl(args.get(0), args.get(1));

		// Arithmetic functions starts here
		case GREATEST:
				return ArithmeticFunctions.getMinOrMax(args, FunctionEnum.GREATEST);
				
		case LEAST:
			return ArithmeticFunctions.getMinOrMax(args, FunctionEnum.LEAST);		
				
		case ABS:
				return new Value(BigDecimal.valueOf(Math.abs(Double.valueOf(args.get(0).toString()))));

		case ACOS:
				return new Value(BigDecimal.valueOf(Math.acos(args.get(0).asDouble())));

		case ASIN:
				return new Value(BigDecimal.valueOf(Math.asin(args.get(0).asDouble())));

		case ATAN:
				return new Value(BigDecimal.valueOf(Math.atan(args.get(0).asDouble())));

		case CEIL:
				return new Value(BigDecimal.valueOf(Math.ceil(args.get(0).asDouble())));

		case COS:
				return new Value(BigDecimal.valueOf(Math.cos(args.get(0).asDouble())));

		case EXP:
				return new Value(BigDecimal.valueOf(Math.exp(args.get(0).asDouble())));

		case FACT:
				return new Value(BigDecimal.valueOf(EvalVisitorHelper.factorial(args.get(0).asInteger().intValue())));

		case FLOOR:
				return new Value(BigDecimal.valueOf(Math.floor(args.get(0).asDouble())));

		case LOG:
				return new Value(BigDecimal.valueOf(Math.log(args.get(0).asDouble())));

		case LOG10:
				return new Value(BigDecimal.valueOf(Math.log10(args.get(0).asDouble())));

		case LOG2:
				return new Value(BigDecimal.valueOf(EvalVisitorHelper.log2(args.get(0).asInteger(), 2)));

		case RAND:
				return new Value(BigDecimal.valueOf(Math.random()));

		case ROUND:
		case ROUNDP:
				return new Value(
					BigDecimal.valueOf(EvalVisitorHelper.round(args.get(0).asDouble(),
							null != args.get(1) ? args.get(1).asInteger() : 0)));

		case SIN:
				return new Value(BigDecimal.valueOf(Math.sin(args.get(0).asDouble())));

		case SQRT:
				return new Value(BigDecimal.valueOf(Math.sqrt(args.get(0).asDouble())));

		case TAN:
				return new Value(BigDecimal.valueOf(Math.tan(args.get(0).asDouble())));

		// Aggregate Functions
		case XAVG:
			 	
		case XMIN:
			 	
		case XMAX:
			 	
		case XSUM:
			 	
		case XCOUNT:

		case XUNIQUECOUNT:
				
		case XFREQCOUNT:
			 	
		case XFREQPERC:
			 	
		case XPERC:
			 	
			return invokeAggregation(args,FunctionEnum.valueOf(functionName));
			
		case INVERSIONCHECK:
				inversionAndRangeCheckFunctions = new InversionAndRangeCheckFunctions(data,otherData);
				return inversionAndRangeCheckFunctions.executeInversionAndRangeCheckFunctions(FunctionEnum.INVERSIONCHECK,args);
		case AGGREGATERANGECHECK:
				inversionAndRangeCheckFunctions = new InversionAndRangeCheckFunctions(data,otherData);
				return inversionAndRangeCheckFunctions.executeInversionAndRangeCheckFunctions(FunctionEnum.AGGREGATERANGECHECK,args);
			
		case TO_STRING: 
				return new Value(args.get(0).toString());
		case TO_NUMBER: 
				return new Value(new BigDecimal(args.get(0).toString()));
			
		default:
				throw new RuntimeException(
					"Unknown Function: " + ExpressionParser.tokenNames[FunctionEnum.valueOf(functionName).ordinal()]);
		}
		}catch(Exception e){
			throw new RuntimeException(
					"Invalid Parameters for function : " +FunctionEnum.valueOf(functionName) + ". SubError : " +e.getMessage());
		}
	}

	/**
	 * @param args
	 * @param functionName 
	 * @return
	 */
	private Value invokeAggregation(List<Value> args, FunctionEnum functionName) {
		
		Value result;
		String expresssionForSearchInCache = FunctionDriver.createExpressionName(functionName, args);

		//Return from aggregateFunctionCache if found
		if (aggregateFunctionCache.containsKey(expresssionForSearchInCache)) {

			logger.debug("result returned for {} from Cache for Aggregation.", expresssionForSearchInCache);

			return aggregateFunctionCache.get(expresssionForSearchInCache);
		} else {
			aggregateFunctions = new AggregateFunctions(this.inputDataMaster, functionName,expression);

			result = CustomFunctions.getDataSpecificValue(aggregateFunctions.aggregationDriver(args));

			if (!aggregateFunctionCache.containsKey(createExpressionName(functionName, args)))
				aggregateFunctionCache.put(createExpressionName(functionName, args), result);

			return result;
		}
	}

	public static String createExpressionName(FunctionEnum functionName, List<Value> args) {
		return functionName + "-" + args;
	}

	private void printArgs(String functionName, List<Value> args) {
		logger.info("{} - {}", functionName, args);
	}

	public static String cleanSingleQuoteStartAndEnd(String str) {
		String input = str;
		boolean endsWithEscapedQuotes = false;
		endsWithEscapedQuotes = (StringUtils.endsWith(input.trim(), "\\\""))
				|| (StringUtils.startsWith(input.trim(), "\\\""));
		
		if (((!StringUtils.endsWith(input.trim(), "\"")) || (!StringUtils.startsWith(input.trim(), "\"")))
				&& (!endsWithEscapedQuotes)) {
			return str;
		}
		if (endsWithEscapedQuotes) {
			input = StringEscapeUtils.unescapeJava(str);
		}
		char[] strChrs = input.toCharArray();
		StringBuilder value = new StringBuilder();
		for (int i = 0; i < strChrs.length; i++) {
			if ((i != 0) && (i + 1 != strChrs.length) && (strChrs[i] != '\"')) {
				value.append(strChrs[i]);
			}
		}
		return value.toString();
	}

}
