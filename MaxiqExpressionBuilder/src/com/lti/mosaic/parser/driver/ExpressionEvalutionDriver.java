package com.lti.mosaic.parser.driver;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.phoenix.schema.FunctionNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lti.mosaic.antlr4.parser.ExpressionLexer;
import com.lti.mosaic.antlr4.parser.ExpressionParser;
import com.lti.mosaic.function.def.Value;
import com.lti.mosaic.parser.ErrorListener;
import com.lti.mosaic.parser.EvalVisitor;
import com.lti.mosaic.parser.exception.ExpressionEvaluatorException;
import com.lti.mosaic.parser.exception.FieldNotValidException;

public class ExpressionEvalutionDriver {

	private static final Logger logger = LoggerFactory.getLogger(ExpressionEvalutionDriver.class);
	
 	// inputDataMaster will contain list of <uuid,data>
	private  Map<String, ArrayList<String[]>> inputDataMaster = new HashMap<>(); 
	private List<String[]> data = new ArrayList<>();
	private  Map<String, Object> otherData = new HashMap<>(); 
	private static final Pattern pattern = Pattern.compile("\"([^\\\\\"]|\\\\\\\\|\\\\\")*\"", Pattern.MULTILINE);
	private Map<String,Value> aggregateFunctionCache = new HashMap<>(); 

	
	public Map<String, ArrayList<String[]>> getInputDataMaster() {
		return inputDataMaster;
	}

	public void setInputDataMaster(Map<String, ArrayList<String[]>> inputDataMaster) {
		this.inputDataMaster = inputDataMaster;
	}

	public List<String[]> getData() {
		return data;
	}

	public void setData(List<String[]> data) {
		this.data = data;
	}

	public void setOtherData(Map<String, Object> otherData) {
		this.otherData = otherData;
	}

	/**
	 * @return the aggregateFunctionCache
	 */
	public Map<String, Value> getAggregateFunctionCache() {
		return aggregateFunctionCache;
	}

	/**
	 * @param aggregateFunctionCache the aggregateFunctionCache to set
	 */
	public void setAggregateFunctionCache(Map<String, Value> aggregateFunctionCache) {
		this.aggregateFunctionCache = aggregateFunctionCache;
	}
	
	/**
	 * Interface function to the outside , evaluates the Logical , math and rest
	 * of function Library
	 * 
	 * @param paramValues
	 * @param expression
	 * @param evaluateWithDummyValues
	 * @return String
	 * 
	 * @exception ExpressionEvaluatorException
	 * 
	 * @author Sarang & Vivek
	 * 
	 * @version 1.0
	 * @throws FieldNotValidException
	 * @throws JSONException 
	 * @throws FunctionNotFoundException 
	 */
	public String expressionEvaluatorWithObject(Map<String, Object> paramValues,
			String expression, boolean evaluateWithDummyValues)
			throws ExpressionEvaluatorException, FieldNotValidException {
		ExpressionLexer lexer = null;
		ExpressionParser parser = null;
		ParseTree tree = null;
		EvalVisitor visitor = null;
		Value value = null;

		try{
		//Get matches of all Strings in expression
		boolean recursiveMatching = true;
		// Replace runtime parameters if present in Quotes, with escaped quotes
		// PE - 7224
		expression = replaceRuntimeParams(paramValues, expression, recursiveMatching);
		// Replace all runtime parameters which are not in Quotes  
		// PE - 7251
		for (Map.Entry<String, Object> entry : paramValues.entrySet()) {
			if (StringUtils.containsIgnoreCase(expression, entry.getKey())) {
				expression = StringUtils.replace(expression, entry.getKey(), entry.getValue().toString());
			}
		}
		
		logger.debug("aggregateFunctionCache:- {}", aggregateFunctionCache);

		logger.debug(expression);
		logger.info("{}","Connecting to ANTLR4.7 parser");
		lexer = new ExpressionLexer(new ANTLRInputStream(expression));
		lexer.addErrorListener(ErrorListener.INSTANCE);
		parser = new ExpressionParser(new CommonTokenStream(lexer));
		parser.addErrorListener(ErrorListener.INSTANCE);
		tree = parser.parse();
		visitor = new EvalVisitor();
		visitor.setCsvData(data, otherData, inputDataMaster,aggregateFunctionCache,expression);
		value = visitor.visit(tree);
		logger.info("{}","Creatinng parser ,tree visitor objets and executing through ANTLR4 grammers ");

		logger.debug("Result : {}", value);
		} catch (Exception e) {
			throw new ExpressionEvaluatorException(e.getMessage(), new Throwable(e.getClass().getPackage() + ""));
		}
		finally {
			if (null != visitor) {
				this.aggregateFunctionCache.putAll(visitor.getFunctionDriver().getAggregateFunctionCache());
			}
		}
		return (null == value) ? null : String.valueOf(value);
	}

	/**
	 * @param paramValues
	 * @param expression
	 * @param recursiveMatching
	 * @return
	 */
	private String replaceRuntimeParams(Map<String, Object> paramValues, String expression, boolean recursiveMatching) {
		boolean match;
		while (recursiveMatching) {
			match = false;
			Matcher matcher = pattern.matcher(expression);
			while (matcher.find()) {
 				for (Map.Entry<String, Object> entry : paramValues.entrySet()) {
					if (matcher.group(0).contains(entry.getKey())) {
						expression = expression.substring(0, matcher.start())
								+ expression.substring(matcher.start(), matcher.end()).replace(entry.getKey(),
										StringEscapeUtils.escapeJava(entry.getValue().toString()))
								+ expression.substring(matcher.end(), expression.length());
 						match = true;
						break;
					}
				}
				if (match) {
					break;
				}
			}
			if(!match){
				break;
			}
		}
		return expression;
	}
	
	public String setCsvData(List<String[]> requestData, Map<String, Object> otherDataInput) {
		String key = UUID.randomUUID().toString().replace("-", "");
		data = requestData;
		otherData = otherDataInput;		
		inputDataMaster.put(key,new ArrayList<String[]>(data));
		return key;
	}
	
	public List<String[]> getCsvData(String uuid){
		return this.inputDataMaster.get(uuid);
	}
	
	public Map<String, Object> getOtherData(){
		return this.otherData;
	}
	
	public boolean removeCsvData(String uuid){
		this.inputDataMaster.remove(uuid);
		return true;
	}	
	
}
