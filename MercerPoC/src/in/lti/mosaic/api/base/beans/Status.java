/**
 * This class will maintain status of each request sumbitted to mosaic api
 * 
 */
package in.lti.mosaic.api.base.beans;

import in.lti.mosaic.api.base.mysql.Id;
import in.lti.mosaic.api.base.mysql.TableName;

/**
 * @author rushikesh
 *
 */

/*
  CREATE TABLE `status` ( `id` int(11) NOT NULL AUTO_INCREMENT, `request_id` varchar(50) DEFAULT
  NULL, `validation_type` varchar(50) DEFAULT NULL, `document_id` varchar(50) DEFAULT NULL, `status` varchar(50) DEFAULT NULL,
  `processing_status` int(11) DEFAULT NULL,
  `environment_name` varchar(50) DEFAULT NULL,`remarks` varchar(5000) DEFAULT NULL,
  `current_time_stamp` varchar(50) DEFAULT NULL, PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8
 */
@TableName(tableName = "status")
public class Status {

  @Id
  private Long id;
  private String requestId;
  private String validationType;
  private String documentId;
  private String statusDescription;
  private Integer processingStatus;
  private Long currentTimeStamp;
  private String environmentName;
  private String remarks;


  /**
   * @return the id
   */
  public Long getId() {
    return id;
  }

  /**
   * @param id the id to set
   */
  public void setId(Long id) {
    this.id = id;
  }

  /**
   * @return the request_id
   */
  public String getRequestId() {
    return requestId;
  }

  /**
   * @param request_id the request_id to set
   */
  public void setRequestId(String requestId) {
    this.requestId = requestId;
  }
  
  /**
   * 
   * @return validation_type
   */
  public String getValidationType() {
    return validationType;
  }

  /**
   * @param validationType the validation_type to set
   */
  public void setValidationType(String validationType) {
    this.validationType = validationType;
  }

  /**
   * @return the document_id
   */
  public String getDocumentId() {
    return documentId;
  }

  /**
   * @param documentId the document_id to set
   */
  public void setDocumentId(String documentId) {
    this.documentId = documentId;
  }

  /**
   * @return the status
   */
  public String getStatusDescription() {
    return statusDescription;
  }

  /**
   * @param status the status to set
   */
  public void setStatusDescription(String status) {
    this.statusDescription = status;
  }
  
  /**
   * @return the processing_status
   */
  public Integer getProcessingStatus() {
    return processingStatus;
  }

  /**
   * @param processingStatus the processing_status to set
   */
  public void setProcessingStatus(Integer processingStatus) {
    this.processingStatus = processingStatus;
  }

  /**
   * @return the current_time_stamp
   */
  public Long getCurrentTimeStamp() {
    return currentTimeStamp;
  }

  /**
   * 
   * @param currentTimeStamp
   */
  public void setCurrentTimeStamp(Long currentTimeStamp) {
    this.currentTimeStamp = currentTimeStamp;
  }

  /**
   * @return the environment_name
   */
  public String getEnvironmentName() {
    return environmentName;
  }

  /**
   * @param environmentName the environment_name to set
   */
  public void setEnvironmentName(String environmentName) {
    this.environmentName = environmentName;
  }

  /**
   * @return the remarks
   */
  public String getRemarks() {
    return remarks;
  }

  /**
   * @param remarks the remarks to set
   */
  public void setRemarks(String remarks) {
    this.remarks = remarks;
  }

}
