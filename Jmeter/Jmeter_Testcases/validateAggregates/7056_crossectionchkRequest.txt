{
  "entities": [
    {
      "contextData": {
        "campaignId": "597b474bfe305700157895c0",
        "companyId": "5a27ca2f7ebf571d70e9b6f6",
        "grpCode": "",
        "cpyCode": "",
        "ctryCode": "BO",
        "orgSize": "",
        "industry": {
          "superSector": "CG",
          "sector": "101",
          "subSector": "2670"
        }
      },
      "sections": [
        {
          "sectionCode": "CD_000",
          "sectionStructure": {
            "columns": []
          },
          "data": [
            {
              "$$id": "5a2803814173ad6246cf10cd",
              "CD_073": null,
              "CD_071": null,
              "CD_045": null,
              "CD_044": null,
              "CD_043": null,
              "CD_006A": null
            }
          ]
        },
        {
          "sectionCode": "EMP_DATA",
          "sectionStructure": {
            "columns": [
              {
                "code": "EMP_282",
                "dataType": "string",
                "questionType": "text",
                "validations": [
                 
                ]
              }
            ]
          },
          "data": [
            {
              "$$id": "5a27e0e14173ad6246cf10ca",
              "POS_CLASS_ADJUST": null,
              "ECONOMIC_VALUE": null,
              "POS_CLASS_INGESTED": null,
              "EMP_154": null,
              "YOUR_GRADE": "",
              "POS_CODE": null,
              "EMP_233": "Yes",
              "EMP_037": "2000",
              "EMP_278": null,
              "EMP_234": null,
              "EMP_036": null,
              "EMP_237": null,
              "EMP_215": null,
              "POS_CLASS": null,
              "YOUR_CODE": "",
              "EMP_379": null,
              "EMP_038": null,
              "EMP_170": null,
              "EMP_172": null,
              "EMP_031": null,
              "EXCLUDE_EMPLOYEE": null,
              "EMP_250": null,
              "EMP_128": "12",
              "EMP_029": null,
              "YOUR_TITLE": "",
              "YOUR_OTHER": "",
              "EMP_242": null,
              "EMP_165": null,
              "EMP_044": null,
              "EMP_285": null,
              "EMP_221": "0",
              "EMP_001": "MK TEST",
              "EMP_125": "1200",
              "EMP_026": null,
              "EMP_245": null,
              "EMP_226": null,
              "EMP_280": null,
              "EMP_282": null,
              "EMP_281": null,
              "EMP_284": null,
              "EMP_262": null,
              "EMP_240": null,
              "EMP_185": null,
              "EMP_283": null,
              "EMP_261": null,
              "POS_TITLE": null,
              "YOUR_LEVEL": "",
              "EMP_316": null,
              "EMP_315": null,
              "YOUR_EEID": "ASD123",
              "EMP_058A": null
            },
            {
              "$$id": "5a27e0e14173ad6246cf10cb",
              "POS_CLASS_ADJUST": null,
              "ECONOMIC_VALUE": null,
              "POS_CLASS_INGESTED": null,
              "EMP_154": null,
              "YOUR_GRADE": "",
              "POS_CODE": null,
              "EMP_233": "Yes",
              "EMP_037": null,
              "EMP_278": null,
              "EMP_234": null,
              "EMP_036": null,
              "EMP_237": null,
              "EMP_215": null,
              "POS_CLASS": null,
              "YOUR_CODE": "",
              "EMP_379": null,
              "EMP_038": null,
              "EMP_170": null,
              "EMP_172": null,
              "EMP_031": null,
              "EXCLUDE_EMPLOYEE": null,
              "EMP_250": null,
              "EMP_128": "12",
              "EMP_029": null,
              "YOUR_TITLE": "",
              "YOUR_OTHER": "",
              "EMP_242": null,
              "EMP_165": null,
              "EMP_044": null,
              "EMP_285": null,
              "EMP_221": null,
              "EMP_001": "MK TEST",
              "EMP_125": "1200",
              "EMP_026": null,
              "EMP_245": null,
              "EMP_226": null,
              "EMP_280": null,
              "EMP_282": null,
              "EMP_281": null,
              "EMP_284": null,
              "EMP_262": null,
              "EMP_240": null,
              "EMP_185": null,
              "EMP_283": null,
              "EMP_261": null,
              "POS_TITLE": null,
              "YOUR_LEVEL": "",
              "EMP_316": null,
              "EMP_315": null,
              "YOUR_EEID": "ASD124",
              "EMP_058A": null
            }
          ]
        },
        {
          "sectionCode": "LTI_Plans",
          "sectionStructure": {
            "columns": []
          },
          "data": [
            {
              "$$id": "5970ef81d4cb50a759a8536b",
              "LTIPLAN02": null,
              "LTIPLAN01": null,
              "LTIPLAN04": null,
              "LTIPLAN05": null,
              "LTIPLAN06": null,
              "LTIPLAN07": null,
              "LTIPLAN08": null,
              "LTIPLAN09": null,
              "LTIPLAN10": null,
              "LTIPLAN11": null,
              "LTIPLAN12": null,
              "LTIPLAN13": null,
              "LTIPLAN14": null,
              "LTIPLAN15": null,
              "LTIPLAN16": null,
              "LTIPLAN17": null,
              "LTIPLAN18": null,
              "LTIPLAN03": null
            }
          ]
        },
        {
          "sectionCode": "COMPEN_000",
          "sectionStructure": {
            "columns": [
              {
                "code": "LPALL_399A",
                "dataType": "string",
                "questionType": "radio_buttons",
                "validations": [
                  {
                    "parameter": "",
                    "expression": "XSUM(\"EMP_223221\",\"\",\"\") == 0 AND this.LPALL_399A.EE_group_13LATAM_01 == \"Y\";",
                    "errorType": "ALERT",
                    "validationType": "expression",
                    "_id": "5a1bef40084bfb00152b84d8",
                    "category": "cross_section",
                    "dependentColumns": [],
                    "dimension": "EE_group_13LATAM_01"
                  },
                  {
                    "parameter": "",
                    "expression": "XSUM(\"EMP_222321\",\"\",\"\") == 0 AND this.LPALL_399A.EE_group_13LATAM_02 == \"Y\";",
                    "errorType": "ALERT",
                    "validationType": "expression",
                    "_id": "5a1bef40084bfb00152b84d8",
                    "category": "cross_section",
                    "dependentColumns": [],
                    "dimension": "EE_group_13LATAM_02"
                  },
                  {
                    "parameter": "",
                    "expression": "XSUM(\"EMP_221\",\"\",\"\") == 0 AND this.LPALL_399A.EE_group_13LATAM_03 == \"Y\";",
                    "errorType": "ALERT",
                    "validationType": "expression",
                    "_id": "5a1bef40084bfb00152b84d8",
                    "category": "cross_section",
                    "dependentColumns": [],
                    "dimension": "EE_group_13LATAM_03"
                  },
                  {
                    "parameter": "",
                    "expression": "XSUM(\"EMP_223331\",\"\",\"\") == 0 AND this.LPALL_399A.EE_group_13LATAM_04 == \"Y\";",
                    "errorType": "ALERT",
                    "validationType": "expression",
                    "_id": "5a1bef40084bfb00152b84d8",
                    "category": "cross_section",
                    "dependentColumns": [],
                    "dimension": "EE_group_13LATAM_04"
                  },
                  {
                    "parameter": "",
                    "expression": "XSUM(\"EMP_221\",\"\",\"\") == 0 AND this.LPALL_399A.EE_group_13LATAM_05 == \"Y\";",
                    "errorType": "ALERT",
                    "validationType": "expression",
                    "_id": "5a1bef40084bfb00152b84d8",
                    "category": "cross_section",
                    "dependentColumns": [],
                    "dimension": "EE_group_13LATAM_05"
                  },
                  {
                    "parameter": "",
                    "expression": "XSUM(\"EMP_221\",\"\",\"\") == 0 AND this.LPALL_399A.EE_group_13LATAM_06 == \"Y\";",
                    "errorType": "ALERT",
                    "validationType": "expression",
                    "_id": "5a1bef40084bfb00152b84d8",
                    "category": "cross_section",
                    "dependentColumns": [],
                    "dimension": "EE_group_13LATAM_06"
                  },
                  {
                    "parameter": "",
                    "expression": "XSUM(\"EMP_221\",\"\",\"\") == 0 AND this.LPALL_399A.EE_group_13LATAM_07 == \"Y\";",
                    "errorType": "ALERT",
                    "validationType": "expression",
                    "_id": "5a1bef40084bfb00152b84d8",
                    "category": "cross_section",
                    "dependentColumns": [],
                    "dimension": "EE_group_13LATAM_07"
                  },
                  {
                    "parameter": "",
                    "expression": "XSUM(\"EMP_221\",\"\",\"\") == 0 AND this.LPALL_399A.EE_group_13LATAM_08 == \"Y\";",
                    "errorType": "ALERT",
                    "validationType": "expression",
                    "_id": "5a1bef40084bfb00152b84d8",
                    "category": "cross_section",
                    "dependentColumns": [],
                    "dimension": "EE_group_13LATAM_08"
                  }
                ],
                "options": {
                  "items": [
                    {
                      "value": "Y",
                      "displayName": "Yes"
                    },
                    {
                      "value": "N",
                      "displayName": "No"
                    },
                    {
                      "value": "NA",
                      "displayName": "Not applicable"
                    }
                  ]
                },
                "dimensions": [
                  "EE_group_13LATAM_01",
                  "EE_group_13LATAM_02",
                  "EE_group_13LATAM_03",
                  "EE_group_13LATAM_04",
                  "EE_group_13LATAM_05",
                  "EE_group_13LATAM_06",
                  "EE_group_13LATAM_07",
                  "EE_group_13LATAM_08"
                ]
              }
            ]
          },
          "data": [
            {
              "$$id": "5a2805c74173ad6246cf10d4",
              "STI_363": null,
              "STI_362": null,
              "STI_361": null,
              "STI_359": null,
              "STI_358": null,
              "STI_356": null,
              "STI_355": null,
              "STI_354": null,
              "LPALL_314": null,
              "LPALL_255": null,
              "LPALL_239": null,
              "LPALL_225": null,
              "LPALL_192": {
                "EE_group_13LATAM_03": "Y"
              },
              "CD_145": [
                "null"
              ],
              "CD_144": null,
              "CD_143": null,
              "CD_142": null,
              "CD_120": null,
              "CD_117": null,
              "CD_116": null,
              "LPALL_399A": {
                "EE_group_13LATAM_01": "Y",
                 "EE_group_13LATAM_02": "Y",
                  "EE_group_13LATAM_03": "Y",
                   "EE_group_13LATAM_04": "Y",
                    "EE_group_13LATAM_05": "Y",
                     "EE_group_13LATAM_06": "Y",
                      "EE_group_13LATAM_07": "Y",
                       "EE_group_13LATAM_08": "Y"
                
              }
            }
          ]
        }
      ]
    }
  ]
}