// Generated from Expression.g4 by ANTLR 4.7.1

package com.lti.mosaic.antlr4.parser;


import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.TerminalNode;

/**
 * This class provides an empty implementation of {@link ExpressionListener},
 * which can be extended to create a listener which only needs to handle a subset
 * of the available methods.
 */
public class ExpressionBaseListener implements ExpressionListener {
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterParse(ExpressionParser.ParseContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitParse(ExpressionParser.ParseContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterBlock(ExpressionParser.BlockContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitBlock(ExpressionParser.BlockContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterAssignmentExpr(ExpressionParser.AssignmentExprContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitAssignmentExpr(ExpressionParser.AssignmentExprContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterIfStatExpr(ExpressionParser.IfStatExprContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitIfStatExpr(ExpressionParser.IfStatExprContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterWhileStatExpr(ExpressionParser.WhileStatExprContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitWhileStatExpr(ExpressionParser.WhileStatExprContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterStatementExpr(ExpressionParser.StatementExprContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitStatementExpr(ExpressionParser.StatementExprContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterOtherExpr(ExpressionParser.OtherExprContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitOtherExpr(ExpressionParser.OtherExprContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterAssignment(ExpressionParser.AssignmentContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitAssignment(ExpressionParser.AssignmentContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterIfStat(ExpressionParser.IfStatContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitIfStat(ExpressionParser.IfStatContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterConditionBlock(ExpressionParser.ConditionBlockContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitConditionBlock(ExpressionParser.ConditionBlockContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterStatBlock(ExpressionParser.StatBlockContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitStatBlock(ExpressionParser.StatBlockContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterWhileStat(ExpressionParser.WhileStatContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitWhileStat(ExpressionParser.WhileStatContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterStatement(ExpressionParser.StatementContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitStatement(ExpressionParser.StatementContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterNotExpr(ExpressionParser.NotExprContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitNotExpr(ExpressionParser.NotExprContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterUnaryMinusExpr(ExpressionParser.UnaryMinusExprContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitUnaryMinusExpr(ExpressionParser.UnaryMinusExprContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterMultiplicationExpr(ExpressionParser.MultiplicationExprContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitMultiplicationExpr(ExpressionParser.MultiplicationExprContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterAtomExpr(ExpressionParser.AtomExprContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitAtomExpr(ExpressionParser.AtomExprContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterOrExpr(ExpressionParser.OrExprContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitOrExpr(ExpressionParser.OrExprContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterAdditiveExpr(ExpressionParser.AdditiveExprContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitAdditiveExpr(ExpressionParser.AdditiveExprContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterPowExpr(ExpressionParser.PowExprContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitPowExpr(ExpressionParser.PowExprContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterRelationalExpr(ExpressionParser.RelationalExprContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitRelationalExpr(ExpressionParser.RelationalExprContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterEqualityExpr(ExpressionParser.EqualityExprContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitEqualityExpr(ExpressionParser.EqualityExprContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterFunctionExpr(ExpressionParser.FunctionExprContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitFunctionExpr(ExpressionParser.FunctionExprContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterAndExpr(ExpressionParser.AndExprContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitAndExpr(ExpressionParser.AndExprContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterZeroParamterFunctions(ExpressionParser.ZeroParamterFunctionsContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitZeroParamterFunctions(ExpressionParser.ZeroParamterFunctionsContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterSingleParamterFunctions(ExpressionParser.SingleParamterFunctionsContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitSingleParamterFunctions(ExpressionParser.SingleParamterFunctionsContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterTwoParamterFunctions(ExpressionParser.TwoParamterFunctionsContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitTwoParamterFunctions(ExpressionParser.TwoParamterFunctionsContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterThreeParamterFunctions(ExpressionParser.ThreeParamterFunctionsContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitThreeParamterFunctions(ExpressionParser.ThreeParamterFunctionsContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterFourParamterFunctions(ExpressionParser.FourParamterFunctionsContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitFourParamterFunctions(ExpressionParser.FourParamterFunctionsContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterNParametersFunctions(ExpressionParser.NParametersFunctionsContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitNParametersFunctions(ExpressionParser.NParametersFunctionsContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterFunctionParam0(ExpressionParser.FunctionParam0Context ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitFunctionParam0(ExpressionParser.FunctionParam0Context ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterFunctionParam1(ExpressionParser.FunctionParam1Context ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitFunctionParam1(ExpressionParser.FunctionParam1Context ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterFunctionParam2(ExpressionParser.FunctionParam2Context ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitFunctionParam2(ExpressionParser.FunctionParam2Context ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterFunctionParam3(ExpressionParser.FunctionParam3Context ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitFunctionParam3(ExpressionParser.FunctionParam3Context ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterFunctionParam4(ExpressionParser.FunctionParam4Context ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitFunctionParam4(ExpressionParser.FunctionParam4Context ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterFunctionParamN(ExpressionParser.FunctionParamNContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitFunctionParamN(ExpressionParser.FunctionParamNContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterArgumentsN(ExpressionParser.ArgumentsNContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitArgumentsN(ExpressionParser.ArgumentsNContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterArguments1(ExpressionParser.Arguments1Context ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitArguments1(ExpressionParser.Arguments1Context ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterArguments2(ExpressionParser.Arguments2Context ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitArguments2(ExpressionParser.Arguments2Context ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterArguments3(ExpressionParser.Arguments3Context ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitArguments3(ExpressionParser.Arguments3Context ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterArguments4(ExpressionParser.Arguments4Context ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitArguments4(ExpressionParser.Arguments4Context ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterParExpr(ExpressionParser.ParExprContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitParExpr(ExpressionParser.ParExprContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterArrayAtom(ExpressionParser.ArrayAtomContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitArrayAtom(ExpressionParser.ArrayAtomContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterNumberAtom(ExpressionParser.NumberAtomContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitNumberAtom(ExpressionParser.NumberAtomContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterBooleanAtom(ExpressionParser.BooleanAtomContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitBooleanAtom(ExpressionParser.BooleanAtomContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterIdAtom(ExpressionParser.IdAtomContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitIdAtom(ExpressionParser.IdAtomContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterStringAtom(ExpressionParser.StringAtomContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitStringAtom(ExpressionParser.StringAtomContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterNilAtom(ExpressionParser.NilAtomContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitNilAtom(ExpressionParser.NilAtomContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterValue_list(ExpressionParser.Value_listContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitValue_list(ExpressionParser.Value_listContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterArrayValues(ExpressionParser.ArrayValuesContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitArrayValues(ExpressionParser.ArrayValuesContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterArrayElementTypes(ExpressionParser.ArrayElementTypesContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitArrayElementTypes(ExpressionParser.ArrayElementTypesContext ctx) { /*do nothing*/}

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterEveryRule(ParserRuleContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitEveryRule(ParserRuleContext ctx) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void visitTerminal(TerminalNode node) { /*do nothing*/}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void visitErrorNode(ErrorNode node) { /*do nothing*/}
}