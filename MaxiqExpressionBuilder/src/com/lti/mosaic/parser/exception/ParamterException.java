package com.lti.mosaic.parser.exception;


/**
 * Licensed Information -- need to work on this 
 */

/**
 * Expception - base expeception 
 * <p/>
 * 
 * @author Piyush
 * @since 1.0
 * @version 1.0
 */


public class ParamterException extends ExpressionEvaluatorException {
	
	private static final long serialVersionUID = 1L;

	public ParamterException(String message, Throwable throwable) {
		super(message, throwable);
	}

	public ParamterException(String message) {
		super(message);
	}
}
